package xyz.p4t.listviewtest;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;


public class MainActivity extends Activity {
    /*
        you will usually want reference to adapter
        especially when you will want to dynamically change
        content of list
     */
    private DickAdapter adapter;
    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ListView list; //my C style is really visible here :p

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new DickAdapter(this);
        list = (ListView) findViewById(R.id.list);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Dick obj;

                //we can also do a parent.getAdapter() and then cast it to our adapter class
                //but why do it when we already have reference to it, lol
                obj = adapter.getItem(position);
                //first, show the dialog so we are 100% sure the layout was created
                dialog.show();
                //get window of dialog, then find the textview inside of it and change it's text
                ((TextView) dialog.getWindow().findViewById(R.id.value)).setText(Integer.toString(obj.size));

            }
        });

        //add data to adapter
        adapter.addAll(new Dick(14), new Dick(10));

        //build the dialog in which we will show the result
        dialog = new AlertDialog.Builder(this)
                .setTitle("My Dong is THAT big")
                .setView(R.layout.row) //just reuse layout from row, will be ugly but hell
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //pass
                    }
                }).create();
    }
}
