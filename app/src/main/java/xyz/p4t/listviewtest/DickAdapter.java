package xyz.p4t.listviewtest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

class DickAdapter extends ArrayAdapter<Dick> {
    DickAdapter(Context context) {
        super(context, -1);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Dick obj;

        if (convertView == null) {
            convertView = ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.row, parent, false);
        }

        obj = getItem(position);

        ((TextView) convertView.findViewById(R.id.value)).setText(Integer.toString(obj.size));

        return convertView;
    }
}
